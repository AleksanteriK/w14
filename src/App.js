import React, { useState, useEffect } from 'react';
import './App.css';

function App() {
  const [messageIndex, updateMessageIndex] = useState(0);
  const [chats, setChats] = useState([]);

  useEffect(() => {
    const storedChats = JSON.parse(localStorage.getItem('chats')) || [];
    setChats(storedChats);
  }, []);

  function showMessages() {
    console.log(chats);
  }

  function sendMessage() {
    const newMessage = document.getElementById('chat').value;
    setChats((prevChats) => [...prevChats, newMessage]);
    localStorage.setItem('chats', JSON.stringify(chats));
    document.getElementById('chat').value = '';
    updateMessageIndex((prevmessageIndex) => prevmessageIndex + 1);
  }

  return (
    <div className="App">
      <input type="text" id="chat" />
      <button onClick={sendMessage}>Send</button>
      <button onClick={showMessages}>Show messages</button>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      A pellentesque sit amet porttitor. Eget aliquet nibh praesent tristique magna sit amet. Condimentum mattis pellentesque id nibh tortor.
      Tellus in metus vulputate eu. Libero justo laoreet sit amet cursus sit amet dictum sit. Gravida neque convallis a cras.
      Venenatis lectus magna fringilla urna porttitor rhoncus. Arcu vitae elementum curabitur vitae nunc sed velit.
      Ut etiam sit amet nisl purus in mollis nunc. Lorem donec massa sapien faucibus et.
      Id leo in vitae turpis massa sed elementum tempus egestas.</p>
      <p>Dictum fusce ut placerat orci. Odio ut sem nulla pharetra diam. Quis lectus nulla at volutpat diam ut. Enim sit amet venenatis urna.
      A arcu cursus vitae congue mauris. Feugiat scelerisque varius morbi enim nunc faucibus. Quis varius quam quisque id diam.
      Tincidunt ornare massa eget egestas purus viverra accumsan in. Ut eu sem integer vitae justo eget magna. Libero justo laoreet sit amet cursus sit.
      Dictum non consectetur a erat nam at. Ut venenatis tellus in metus vulputate eu scelerisque felis. Amet massa vitae tortor condimentum.</p>
      <p>Neque viverra justo nec ultrices dui sapien eget mi. Id aliquet lectus proin nibh nisl condimentum id venenatis. Nunc id cursus metus aliquam eleifend mi in nulla.
      Ipsum dolor sit amet consectetur adipiscing elit duis. Tortor vitae purus faucibus ornare suspendisse sed nisi lacus. Nisl suscipit adipiscing bibendum est ultricies integer.
      Dignissim convallis aenean et tortor at risus viverra. Blandit massa enim nec dui nunc mattis enim ut. Lectus arcu bibendum at varius.
      Aliquet risus feugiat in ante metus dictum at tempor. Risus ultricies tristique nulla aliquet enim tortor. Scelerisque purus semper eget duis at tellus at.
      Integer malesuada nunc vel risus commodo viverra maecenas. Porttitor massa id neque aliquam vestibulum morbi. Nibh tellus molestie nunc non blandit.</p>
      <p>Ac orci phasellus egestas tellus rutrum tellus. Vitae justo eget magna fermentum iaculis eu non diam. Ultrices tincidunt arcu non sodales neque sodales ut etiam sit. Convallis posuere morbi leo urna molestie at elementum.
      Tristique senectus et netus et malesuada fames ac turpis. Pellentesque habitant morbi tristique senectus et netus et. Aliquam sem et tortor consequat id porta nibh venenatis cras.
      Quisque sagittis purus sit amet volutpat consequat mauris. Pharetra magna ac placerat vestibulum lectus mauris. Amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar.
      Morbi tristique senectus et netus et. Mauris nunc congue nisi vitae. Sed faucibus turpis in eu mi bibendum neque. Sapien eget mi proin sed libero. Amet purus gravida quis blandit turpis cursus.
      Mattis rhoncus urna neque viverra justo nec ultrices. Aliquet risus feugiat in ante metus dictum at tempor commodo. Tempor orci eu lobortis elementum nibh tellus.</p>
      <p>Eget egestas purus viverra accumsan in nisl nisi scelerisque. Nibh sed pulvinar proin gravida. Faucibus turpis in eu mi bibendum neque. Varius sit amet mattis vulputate enim nulla.
       Adipiscing vitae proin sagittis nisl. Purus sit amet volutpat consequat mauris. Massa placerat duis ultricies lacus sed turpis. Etiam tempor orci eu lobortis elementum nibh. Vel quam elementum pulvinar etiam non.
      Adipiscing enim eu turpis egestas pretium aenean pharetra magna ac. Elit duis tristique sollicitudin nibh sit amet. Sed odio morbi quis commodo odio aenean sed. Amet facilisis magna etiam tempor orci eu lobortis elementum nibh.</p>
    </div>
  );
}

export default App;
